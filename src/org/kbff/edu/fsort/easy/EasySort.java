package org.kbff.edu.fsort.easy;

import java.util.ArrayList;
import java.util.Collections;

public class EasySort {
	/**
	 * Возвращает список пар "Число:Количество вхождений", подсчитанных в массиве
	 * Информация о вхождениях для каждого из чисел описана объектом Pair
	 * 
	 * @param array исходный массив
	 * @return список пар "Число:Количество вхождений"
	 */
	public static ArrayList<Pair> countNumbersFrequency(int[] array) {
		ArrayList<Pair> pairs = new ArrayList<Pair>();
		for(int number : array) {
			//формируем данные о вхождении
			Pair p = new Pair(number);
			/*
			 * если такое число уже встречалось в массиве
			 * информация о его вхождениях обязательно будет находиться 
			 * в списке pairs
			 */
			int i = pairs.indexOf(p);
			if(i == -1) {
				//Ан нет, это новое для нас число
				//просто сохраняем новые данные в конце списка
				pairs.add(p);
			} else {
				//увеличиваем счётчик вхождений для этого числа
				pairs.get(i).count++;
			}
		}
		return pairs;
	}
	
	/**
	 * Сортирует переданный массив по кол-ву вхождений тех или иных чисел 
	 * 
	 * Алгоритм:
	 * 1. Определить кол-во вхождений каждого числа (см метод countNumbersFrequency)
	 * 2. Отсортировать массив, хранящий данные о вхождениях по убыванию
	 * 3. Записать данные в отсортированном порядке в массив 
	 * 
	 * @param array исходный массив
	 * @return отсортированный по числу вхождений элементов массив
	 */
	@SuppressWarnings("unchecked")
	public static int[] sort(int[] array) {
		ArrayList<Pair> frequency = countNumbersFrequency(array);
		Collections.sort(frequency);
		
		int offset = 0;
		for(Pair pair : frequency) {
			pair.write(array, offset);
			offset += pair.count;
		}
		
		return array;
	}
}
