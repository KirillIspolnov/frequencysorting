package org.kbff.edu.fsort;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

import org.kbff.edu.fsort.alco.AlcoSort;
import org.kbff.edu.fsort.easy.EasySort;

public class Launcher {
	private static final int SORT_ALCOHOL_BOTTLE = 1;
	private static final int SORT_EASY_AND_SMART = 2;
	private static Scanner scanner = new Scanner(System.in);
	
	public static boolean initScanner() {
		try(FileInputStream fis = new FileInputStream("/input.txt");
			BufferedInputStream bis = new BufferedInputStream(fis)) {
			scanner = new Scanner(bis);
		} catch(IOException ioexc) {
			ioexc.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public static int[] readInputData() {
		int[] array = new int[scanner.nextInt()];
		int i = 0;
		while(scanner.hasNextInt() && i < array.length) {
			array[i++] = scanner.nextInt();
		}
		return array;
	}
	
	public static String stringify(int[] output) {
		StringBuilder builder = new StringBuilder();
		for(int element : output) {
			builder.append(element).append(" ");
		}
		return builder.toString();
	}
	
	public static boolean writeResults(int[] output) {
		try(FileOutputStream fos = new FileOutputStream("/output.txt");
				BufferedOutputStream bos = new BufferedOutputStream(fos)) {
			
			String data = stringify(output);
			bos.write(data.getBytes());
		} catch(IOException ioexc) {
			ioexc.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		System.out.println("Какой из методов сортировки использовать?");
		System.out.printf("%d - метод \"без 100 грамм не разберёшься\"", SORT_ALCOHOL_BOTTLE);
		System.out.printf("%d - метод \"опять ООП, ну ё-маё...\"", SORT_EASY_AND_SMART);
		
		int mode = scanner.nextInt();
		while(mode != SORT_ALCOHOL_BOTTLE || mode != SORT_EASY_AND_SMART) {
			System.out.print("Не понял. Давай либо 1, либо 2: ");
			mode = scanner.nextInt();
		}
		
		System.out.println("Слава Богам, выбор сделан... А теперь дай спокойно подготовить инструменты для лоботомии, ой, для чтения файла...");
		if(initScanner()) {
			System.out.println("Эх, ладно, сегодня Боги благосклонны к тебе...");
		} else {
			System.err.println("Тут холодно. Сделай мне глинтвейн, а то я заболею. И проверь наличие файла! Ну, это так, не обязательно...");
			return;
		}
		
		int[] input = readInputData();
		int[] output;
		switch(mode) {
			case SORT_ALCOHOL_BOTTLE:
				output = AlcoSort.sort(input);
				break;
			case SORT_EASY_AND_SMART:
				output = EasySort.sort(input);
				break;
			default:
				System.out.println("Ты как сюда пролез(-ла)?!");
				return;
		}
		
		if(writeResults(output)) {
			System.out.println("Готово. Спасибо не булькает, как говорится...");
		} else {
			System.err.println("Иди карму чисть! Что-то не так пошло...");
		}
	}
}
