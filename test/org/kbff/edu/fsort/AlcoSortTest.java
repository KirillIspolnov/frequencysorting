package org.kbff.edu.fsort;

import static org.junit.Assert.*;

import org.junit.Test;
import org.kbff.edu.fsort.alco.AlcoSort;
import org.kbff.edu.fsort.helpers.IndexTestKit;
import org.kbff.edu.fsort.helpers.SortTestKit;
import org.kbff.edu.fsort.helpers.TestDataContainer;

public class AlcoSortTest {
	private SortTestKit[] sortSuite;
	private IndexTestKit[] indexSuite;
	
	public AlcoSortTest() {
		TestDataContainer.loadIfNeeded();
		sortSuite = TestDataContainer.getSuiteForSort();
		indexSuite = TestDataContainer.getSuiteForIndex();
	}
	
	@Test
	public void testSort() {
		for(SortTestKit stk : sortSuite) {
			assertArrayEquals(stk.expected, AlcoSort.sort(stk.origin));
			System.out.println("Sort test executed...");
		}
	}

	@Test
	public void testIndex() {
		for(IndexTestKit kit : indexSuite) {
			int index = AlcoSort.indexOfNumberInFrequency(kit.origin, kit.target);
			assertEquals(kit.expected, index);
			System.out.println("IndexOf test executed...");
		}
	}
}
