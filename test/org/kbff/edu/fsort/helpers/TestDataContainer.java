package org.kbff.edu.fsort.helpers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class TestDataContainer {
	private static boolean loaded;
	public static JsonArray sortTestSuite;
	public static JsonArray indexTestSuite;
	
	public static void loadIfNeeded() {
		if(loaded) return;
		
		try(FileReader freader = new FileReader("tests.json");
			BufferedReader breader = new BufferedReader(freader)) {
			JsonParser jparser = new JsonParser();
			JsonObject root = jparser.parse(breader).getAsJsonObject();
			
			sortTestSuite = root.get("sort").getAsJsonArray();
			indexTestSuite = root.get("index").getAsJsonArray();
		} catch(IOException ioe) {
			System.out.println("Не получилось...");
			ioe.printStackTrace();
		}
	}
	
	public static SortTestKit[] getSuiteForSort() {
		SortTestKit[] suite = new SortTestKit[sortTestSuite.size()];
	
		for(int i = 0; i < sortTestSuite.size(); i++) {
			JsonObject kit_raw = sortTestSuite.get(i).getAsJsonObject();
			
			JsonArray jorigin = kit_raw.get("origin").getAsJsonArray();
			JsonArray jexpected = kit_raw.get("expected").getAsJsonArray();
			
			int[] origin = new int[jorigin.size()];
			int[] expected = new int[jorigin.size()];
			
			for(int j = 0; j < origin.length; j++) {
				origin[j] = jorigin.get(j).getAsInt();
				expected[j] = jexpected.get(j).getAsInt();
			}
			
			suite[i] = new SortTestKit(origin, expected);
		}
		
		return suite;
	}

	public static IndexTestKit[] getSuiteForIndex() {
		IndexTestKit[] suite = new IndexTestKit[indexTestSuite.size()];
		
		for(int i = 0; i < indexTestSuite.size(); i++) {
			JsonObject kit_raw = indexTestSuite.get(i).getAsJsonObject();
			
			JsonArray jfrequency = kit_raw.get("frequency").getAsJsonArray();
			int target = kit_raw.get("target").getAsInt();
			int expected = kit_raw.get("expected").getAsInt();
			
			int[][] frequency = new int[jfrequency.size()][2];
			for(int j = 0; j < frequency.length; j++) {
				JsonArray local_array = jfrequency.get(j).getAsJsonArray();
				frequency[j] = new int[]{ 
						local_array.get(0).getAsInt(), 
						local_array.get(1).getAsInt() };
			}
			
			suite[i] = new IndexTestKit(frequency, target, expected);
		}
		
		return suite;
	}
}
