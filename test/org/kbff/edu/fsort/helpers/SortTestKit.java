package org.kbff.edu.fsort.helpers;

public class SortTestKit {
	public int[] origin, expected;
	
	public SortTestKit(int[] origin, int[] expected) {
		this.origin = origin;
		this.expected = expected;
	}
}
