package org.kbff.edu.fsort.helpers;

public class IndexTestKit {
	public int[][] origin;
	public int target, expected;
	
	public IndexTestKit(int[][] origin, int target, int expected) {
		this.origin = origin;
		this.target = target;
		this.expected = expected;
	}
}
