package org.kbff.edu.fsort;

import static org.junit.Assert.*;

import org.junit.Test;
import org.kbff.edu.fsort.easy.EasySort;
import org.kbff.edu.fsort.helpers.SortTestKit;
import org.kbff.edu.fsort.helpers.TestDataContainer;

public class EasySortTest {
	private SortTestKit[] sortSuite;
	
	public EasySortTest() {
		TestDataContainer.loadIfNeeded();
		sortSuite = TestDataContainer.getSuiteForSort();
	}
	
	@Test
	public void testSort() {
		for(SortTestKit kit : sortSuite) {
			assertArrayEquals(kit.expected, EasySort.sort(kit.origin));
			System.out.println("Sort test executed...");
		}
	}
}
