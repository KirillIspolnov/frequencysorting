package org.kbff.edu.fsort.easy;

/**
 * Comparable = Сравниваемые
 * Этот интерфейс описывает объекты, которые можно сравнить между 
 * собой по какому-то свойству (свойствам)
 * 
 * @author Kirill Ispolnov, 16it18k
 */
@SuppressWarnings("rawtypes")
public class Pair implements Comparable {
	//число
	public int number;
	//кол-во вхождений числа
	public int count;
	
	public Pair(int number) {
		this.number = number;
		this.count = 1;
	}
	
	/**
	 * Записывает последовательность из count чисел number в массив array
	 * 
	 * @param array массив для хранения данных
	 * @param offset позиция для начала записи
	 */
	public void write(int[] array, int offset) {
		for(int i = 0; i < count; i++) {
			array[offset + i] = number;
		}
	}
	
	public boolean equals(Object other) {
		return other instanceof Pair && ((Pair)other).number == number;
	}
	
	/**
	 * Выполняет сравнение двух объектов и возвращает результат сравнения в слеующем формате
	 * -1 если переданный объект меньше
	 * 0 если два объекта равны
	 * +1 если переданный объект больше
	 */
	public int compareTo(Object other) {
		/* мы не сможем точно определить какой из объектов больше
		   если переданный объект не будет являться экземпляром
		   класса Pair
		*/
		if(other instanceof Pair) {
			Pair pair = (Pair)other;
			
			//сравнение по кол-ву вхождений
			if(pair.count < count) {
				return -1;
			} else if(pair.count > count) {
				return 1;
			}
		}
		
		return 0;
	}
}
